#!/bin/awk -f

# Simple tool to list dependencies in form suitable for tsort utility.
# Run this script like this:
#   /some/path/list-dependencies-from-patches.awk *.sql | tsort | tac
# To get patches in order that satisfies dependencies while loading them.

tolower($0) ~ /^[[:space:]]*select[[:space:]]+_v.register_patch\(/ {
    if(!match($0, /\([[:space:]]*'/)) {
        print "warning: malformed register_patch line: "FILENAME":"FNR >"/dev/stderr"
        print "no patch name found" >"/dev/stderr"
        next
    }
    line_remaining = substr($0, RSTART + RLENGTH)

    if(!match(line_remaining, /'/)) {
        print "warning: malformed register_patch line: "FILENAME":"FNR >"/dev/stderr"
        print "not end of patch name" >"/dev/stderr"
        next
    }
    patch_name = substr(line_remaining, 1, RSTART - 1)
    line_remaining = substr(line_remaining, RSTART + RLENGTH)

    print patch_name " " patch_name;
    if(!match(line_remaining, /[[:space:]]*,[[:space:]]*ARRAY\[[[:space:]]*'/)) {
        next
    }
    line_remaining = substr(line_remaining, RSTART + RLENGTH)
    while(match(line_remaining, /[[:space:]]*,[[:space:]]*ARRAY\[[[:space:]]*'/)) {
        print patch_name " " substr(line_remaining, 1, RSTART - 1)
        line_remaining = substr(line_remaining, RSTART + RLENGTH)
    }
    if(!match(line_remaining, /'/)) {
        print "warning: malformed register_patch line: "FILENAME":"FNR >"/dev/stderr"
        print "not end of dependency name" >"/dev/stderr"
        next
    }
    print patch_name " " substr(line_remaining, 1, RSTART - 1)
}
